package snippets

import "fmt"

func FindAnagrams(s, p string) []int {
    if len(s) < len(p) {
        return []int{}
    }
    res := []int{}
    start, end := 0, 0
    lenP := len(p)

    windowRec := make(map[rune]int)
    for _, c := range p {
        if cnt, ok := windowRec[c]; ok {
            windowRec[c] = cnt+1
        } else {
            windowRec[c] = 1
        }
    }
    numNotMatch := len(windowRec)

    for ; end < len(s); {
        thisRune := rune(s[end])
        if cnt, ok := windowRec[thisRune]; !ok {
            start = end+1
            end = start

            // reset the state
            numNotMatch = len(windowRec)
            windowRec = make(map[rune]int)
            for _, c := range p {
                if cnt, ok := windowRec[c]; ok {
                    windowRec[c] = cnt+1
                } else {
                    windowRec[c] = 1
                }
            }
            continue
        } else {
            if end - start < lenP {
                end ++
                windowRec[thisRune] = cnt-1
                if cnt == 1 {
                    numNotMatch --
                } else if cnt == 0 {
                    numNotMatch ++
                }
            } else {

                windowRec[thisRune] = cnt-1
                start_cnt, _ := windowRec[rune(s[start])]
                windowRec[rune(s[start])] = start_cnt+1

                if s[start] != s[end] {
                    if cnt == 1 {
                        numNotMatch --
                    } else if cnt == 0 {
                        numNotMatch ++
                    }
                    if start_cnt == -1 {
                        numNotMatch --
                    } else if start_cnt == 0 {
                        numNotMatch ++
                    }
                }
                end ++
                start ++
            }
            if numNotMatch == 0 {
                res = append(res, start)
            }
        }
        fmt.Println(windowRec, numNotMatch)
    }
    return res
}
