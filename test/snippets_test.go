package test

import (
    "gitlab.com/xingchijin/coding_in_general/snippets"
    "testing"
    "fmt"
)

func Test_findAnagrams(t *testing.T) {
    testCases := [][]string {
        {"cbaebabacd", "abc"},
        {"abab", "ab"},
    }

    for i, testcase := range testCases {
        res := snippets.FindAnagrams(testcase[0], testcase[1])
        fmt.Printf("TestCase %d: %v\n", i+1, res)
    }
}
