package swagger
import "io"

func GetChatRoomIdFromURL(url string) string {
    // skip implementation here, simple string parsing
    return "0101010101"
}

func GetUserIdFromPayload(body io.ReaderCloser) string {
    // skip implementation here, simple string parsing
    return "0202020202"
}
func GetMessageFromPayload(body io.ReaderCloser) string {
    // skip implementation here, simple string parsing
    return &Message{
        UserId: "0202020202",
        Time: "2021-03-10",
        Message: "this is test message",
    }
}
