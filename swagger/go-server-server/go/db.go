package swagger

import "sync"
var oneRoom *ChatRoom = &ChatRoom {
                            name: "example",
                            id: "000000",
                            users: make(map[string]*User),
                            messages: make([]*Message, 0),
                      }

func SearchChatRoom(id string) *ChatRoom {
    // Because no implementing creating chat room, just return one room
    return oneRoom
}

type User struct {
    name string
    id string
    lastRetrieveTime string
}

type ChatRoom struct {
    name string
    id string
    users map[string]*User
    mu sync.Mutex
    userMu sync.Mutex
    messages []string
}

func (c *ChatRoom) GetMessagesForUser(user *User) []*Message {
    lastTime := user.lastRetrieveTime

    startIndex := 0
    // Here we can use binary search
    for i, m := range c.messages {
        if m.Time > lastTime {
            startIndex = i
            break
        }
    }
    return c.messages[startIndex:]
}

func (c *ChatRoom) JoinRoom(u *User) {
    userMu.Lock()
    c.users[u.id] = u
    userMu.Unlock()
}

func (c *ChatRoom) LeaveRoom(id string) {
    userMu.Lock()
    delete(c.users, id)
    userMu.Unlock()
}

func (c *ChatRoom) AddMessage(m *Message) {
    if m == nil {
        return
    }
    c.mu.Lock()
    c.messages = append(c.messages, m)
    // we need to sort the messages by time, skip here, this is just for explanation of the idea
    c.mu.Unlock()
}
